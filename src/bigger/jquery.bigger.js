"use strict";

$.fn.bigger = function(options) {

	var plugin = this,
			defaults = {
				biggerArea : "#biggerArea",
				biggerClass : plugin.selector,
				magnification : 150
			};

	plugin.settings = {};

	plugin.init = function(){

		plugin.settings = $.extend( {}, defaults, options);

		$(plugin.settings.biggerArea).css({
			"width": "100%",
		  "height": "100%",
		  "visibility": "hidden",
		  "position": "fixed",
		  "top": 0,
		  "left": 0,
		  "z-index": 9999
		});

		// preload
		var manifest = [];
		$(plugin.settings.biggerClass).each(function(index, el) {
			manifest.push($(this).attr('src'));
		});
		var promises = [];
		for (var i = 0; i < manifest.length; i++) {
		    (function(url, promise) {
		        var img = new Image();
		        img.onload = function() {
		          promise.resolve();
		        };
		        img.src = url;
		    })(manifest[i], promises[i] = $.Deferred());
		}
		$.when.apply($, promises).done(function() {
		  	bigger();
		});
	}

	var bigger = function(){

		var areaObj = $(plugin.settings.biggerArea),
				ww = $(window).width(),
				wh = $(window).height(),
				imgW = "",
				imgH = "";

		areaObj.css({"width" : ww,"height" : wh});

		$(document).on('click', plugin.settings.biggerClass, function(event) {
			var imgObj = "<img src='" + $(this).attr('src') + "'>";
			areaObj.append(imgObj).css({"visibility" : "visible"});
			areaObj.find("img").css({
				"position" : "absolute",
				'width' : String(plugin.settings.magnification) + "%"
			});
			imgW = areaObj.find("img").width();
			imgH = areaObj.find("img").height();
		});

		$(document).on('click', plugin.settings.biggerArea, function(event) {
			areaObj.css({"visibility" : "hidden"}).find("img").remove();
		});

		// Mouse Move
		if(document.addEventListener){	// イベントリスナーに対応している
			document.addEventListener("mousemove" , MouseMove);
		}else if(document.attachEvent){	// アタッチイベントに対応している
			document.attachEvent("onmousemove" , MouseMove);
		}

		function MouseMove(e){
			var imgX = (imgW - ww) * ((e.clientX / ww * 100) / 100),
					imgY = (imgH - wh) * ((e.clientY / wh * 100) / 100);
			areaObj.find("img").css({"top" : -imgY,"left" : -imgX});
		}
	}

	plugin.init();

};
